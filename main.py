#!/usr/bin/env python3

import socket, os, sys, traceback, _thread

from conn import handleConn, initConnHandler

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

if len(sys.argv) != 4:
	print("You need to include exactly three arguments: the host, \
the port, and the data directory (you provided %s)"%(len(sys.argv) - 1))
	sys.exit(1)

try:
	os.chdir(sys.argv[3])
except Exception as e:
	print("Failed to change to the specified directory ('%s')"%sys.argv[3])
	print("Error: %s"%e)
	sys.exit(1)

try:
	port = int(sys.argv[2])
except:
	print("The port you provided is not a valid number")
	sys.exit(1)

sock_addr = (sys.argv[1], port)

try:
	sock.bind(sock_addr)

	# You might be wondering why the backlog size is so big.
	# Well, the reason is that I don't want to deal with multi-threading.
	# The marginal benefit is not worth the bugs that it would cause.
	# So I just go through the requests one by one.
	sock.listen(100)
except Exception as e:
	print("Failed to initialise socket: %s"%e)
	sys.exit(1)

def connHandler(conn):
	try:
			handleConn(conn, addr)
	except Exception:
		traceback.print_exc()
		conn.send(b"204\n")
	conn.close()

try:
	initConnHandler()
	while True:
		conn, addr = sock.accept()
		print("Received new connection from '%s'"%addr[0])
		_thread.start_new_thread(connHandler, (conn,))
except Exception as e:
	print("Uncaught fatal error: %s"%e)
except KeyboardInterrupt:
	print("Interrupted, quitting")
finally:
	print("Closing socket...")
	sock.close()
