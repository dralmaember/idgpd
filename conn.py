import sqlite3 as sq3, sys, time, socket, re, hashlib

TIMEOUT_SECS = 2

grp_id_regex = None
usrname_regex = None


def isValidGroupId(id):
	return grp_id_regex.match(id) and 1 <= len(id) <= 256
def isValidUserName(usrname):
	return usrname_regex.match(usrname) and 1 <= len(usrname) <= 256

def processUsername(username):
	if "#" not in username:
		return username
	hashidx = username.index("#")
	hashed = hashlib.sha256(username.encode("utf-8")).hexdigest()
	return username[:hashidx] + "#" + hashed

def initConnHandler():
	global grp_id_regex, usrname_regex
	grp_id_regex = re.compile(R"^([0-9a-z]([0-9a-z\-\.]?[0-9a-z])*)?[0-9a-z]?$")
	usrname_regex = re.compile(R"^[0-9a-z]([0-9a-z\-]*[0-9a-z])?(\#.+)?$")

	try:
		sq_conn = sq3.connect("messages.sqlite3")
		table_create_str = """
		CREATE TABLE IF NOT EXISTS posts (
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			author TEXT,
			subject TEXT,
			group_id TEXT,
			posted_at DATETIME,
			contents TEXT
		);
		"""

		crs = sq_conn.cursor()
		crs.execute(table_create_str)

		crs.execute("SELECT count(id) FROM posts")
		if crs.fetchone()[0] == 0:
			sqlc = """INSERT INTO
			          posts(id, author,   subject,    group_id,      posted_at, contents)
			          VALUES(0, 'SYSTEM', 'FALLBACK', '*internal*', '1970-01-01T00:00:00',
			          'You are not meant to see this.');"""
			crs.execute(sqlc)
	except sq3.Error as e:
		print("Failed to connect to database: %s"%e)
		sys.exit(1)

def handleGet(header, conn, startTime, sq_conn):
	header = header.split(" ", 2)
	if len(header) < 3 or not isValidGroupId(header[1]):
		conn.send(b"200\n")
		return
	try:
		postid = int(header[2])
	except ValueError:
		invalid = True
	else:
		invalid = postid < 0
	if invalid:
		conn.send(b"200\n")
		return

	if postid == 0:
		try:
			file = open("desc/" + header[1])
		except FileNotFoundError:
			conn.send(b"101 IDGP-DAEMON There is no description available for the requested group\n")
			return

		contents = file.read().encode("utf-8")
		file.close()
		author = "IDGP-DAEMON"
		subject = "Description of %s"%(header[1])
	else:
		sql = """
		      SELECT author, subject, contents FROM posts WHERE id = ? AND group_id = ?
		      """
		crs = sq_conn.cursor()
		crs.execute(sql, (postid, header[1]))
		post = crs.fetchone()
		if post is None:
			conn.send(b"100 IDGP-DAEMON The requested post was nowhere to be found\n")
			return
		author, subject, contents = post[0], post[1], post[2]

	conn.send(("000 %s %s\n"%(author, subject)).encode("utf-8"))
	conn.send(contents)

def handlePost(header, conn, startTime, sq_conn):
	header = header.split(" ", 4)
	if len(header) < 5:
		conn.send(b"200\n")
		return
	header = header[1:]
	author = header[0]
	group = header[1]
	bodyLen = header[2]
	subject = header[3]

	valid = True
	if not isValidUserName(author):
		valid = False
	elif not isValidGroupId(group):
		valid = False

	try:
		bodyLen = int(bodyLen)
	except ValueError:
		valid = False
	else:
		if bodyLen > 2**64 - 1:
			valid = False

	if not valid:
		conn.send(b"200\n")
		return

	author = processUsername(author)

	body = b""
	nread = 0
	while time.time() <= startTime + TIMEOUT_SECS and nread < bodyLen:
		try:
			new = conn.recv(1, socket.MSG_DONTWAIT)
		except OSError:
			pass
		else:
			body += new
			nread += 1
	if time.time() > startTime + TIMEOUT_SECS or nread < bodyLen:
		conn.send(b"203")
		return

	storeReq = """
	           INSERT INTO posts (author, subject, group_id, posted_at, contents)
	           VALUES            (?,      ?,       ?,        ?,         ?)
	           """
	crs = sq_conn.cursor()
	crs.execute(storeReq, (author, subject, group, time.time(), body))
	sq_conn.commit()
	conn.send(("000 %s Posted successfully\n"%(crs.lastrowid)).encode("utf-8"))

def handleList(header, conn, startTime, sq_conn):
	header = header.split(" ", 3)
	if len(header) < 4:
		conn.send(b"200\n")
		return
	header = header[1:]
	group, offset, count = header[0], header[1], header[2]
	if not isValidGroupId(group):
		conn.send(b"200\n")
		return
	try:
		offset = int(offset)
		count = int(count)
	except ValueError:
		invalid = True
	else:
		invalid = offset < 0 or count < 0

	if invalid:
		conn.send(b"200\n")
		return

	sql = """
	      SELECT id, author, subject FROM posts
	      WHERE id <= ? AND group_id = ? ORDER BY id DESC LIMIT ?
	      """
	if offset == 0:
		sql = """
		      SELECT id, author, subject FROM posts
		      WHERE group_id = ? ORDER BY id DESC LIMIT ?
		      """
	crs = sq_conn.cursor()
	if offset != 0:
		crs.execute(sql, (offset, group, count))
	else:
		crs.execute(sql, (group, count))
	rows = crs.fetchall()
	if len(rows) == 0:
		conn.send(b"102 This group is empty\n")
		return

	for row in rows:
		conn.send(("%s %s %s\n"%row).encode("utf-8"))

def handleListGroups(header, conn, startTime, sq_conn):
	if len(header) != 2:
		conn.send(b"200\n")
		return

	sql = """
	      SELECT DISTINCT group_id FROM posts WHERE id <> 0 ORDER BY group_id
	      """
	crs = sq_conn.cursor()
	crs.execute(sql)
	rows = crs.fetchall()
	for row in rows:
		conn.send(row[0].encode("utf-8") + b"\n")

def handleConn(conn, addr):
	startTime = time.time()
	header = b""
	new = b""
	while time.time() <= startTime + TIMEOUT_SECS:
		try:
			new = conn.recv(1, socket.MSG_DONTWAIT)
		except OSError:
			pass
		if len(header) != 0 and new == b"\n":
			break
		else:
			header += new
	if time.time() > startTime + TIMEOUT_SECS:
		conn.send(b"203\n")
		return

	header = header.decode("utf-8")
	if len(header) < 2 or header[1] != " ":
		conn.send(b"200\n")
		return
	method = header[0]
	if method not in "GPLT":
		conn.send(b"200\n")
		return
	sqConn = sq3.connect("messages.sqlite3")
	handlers = {"G": handleGet, "P": handlePost, "L": handleList, "T": handleListGroups}
	isTimeout = handlers[method](header, conn, startTime, sqConn)
